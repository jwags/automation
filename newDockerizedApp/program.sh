#!/bin/bash
# The script will fail at the first error encountered
set -e

# Create user
echo "Enter deployment account name (deployment-projectname)"
read serviceAccount


if [ ! -d "/home/$serviceAccount" ]; then
  adduser $serviceAccount
  usermod -a -G $serviceAccount root
  usermod -a -G $serviceAccount apache
  usermod -a -G $serviceAccount jwagner
  usermod -a -G docker $serviceAccount
  echo "Match User $serviceAccount" >> /etc/ssh/sshd_config
  echo -e "\tPasswordAuthentication no" >> /etc/ssh/sshd_config

  # Setup ssh
  mkdir /home/$serviceAccount/.ssh
  touch /home/$serviceAccount/.ssh/authorized_keys
  chmod 700 /home/$serviceAccount/.ssh
  chmod 600 /home/$serviceAccount/.ssh/authorized_keys

  ssh-keygen -b 2048 -t rsa -f /home/$serviceAccount/.ssh/id_rsa -q -N ""
  chmod 600 /home/$serviceAccount/.ssh/id_rsa
  chmod 644 /home/$serviceAccount/.ssh/id_rsa.pub
  chown -R $serviceAccount:$serviceAccount /home/$serviceAccount/.ssh

  cat /home/$serviceAccount/.ssh/id_rsa.pub >> /home/$serviceAccount/.ssh/authorized_keys
fi

# Create Virtual Host
echo "Enter production domain (SubDomain.JordansPortfolio.com)"
read prodDomain 

echo "Enter stage domain (SubDomainStage.JordansPortfolio.com)"
read devDomain

mkdir /var/www/$prodDomain
mkdir /var/www/$prodDomain/prod
mkdir /var/www/$prodDomain/prod/wwwroot
mkdir /var/www/$prodDomain/prod/Logs
mkdir /var/www/$prodDomain/prod/Logs/httpd
mkdir /var/www/$prodDomain/stage
mkdir /var/www/$prodDomain/stage/wwwroot
mkdir /var/www/$prodDomain/stage/Logs
mkdir /var/www/$prodDomain/stage/Logs/httpd

# Get next port
#                    echo contents of all available sites      Match the port numbers and spit them out                         Sort ports         Take highest and trim whitespace
currentHighestPort=$(cat /etc/httpd/sites-available/* | sed -rn 's/ProxyPass \/ http:\/\/127.0.0.1:([0-9]+)(.*)$/\1/p' | sort -t= -nr -k3 | head -1 | xargs)
stagePortNumber=$((currentHighestPort + 1))
prodPortNumber=$((stagePortNumber + 1))

export port=$prodPortNumber env="prod" domain=$(echo "$prodDomain" | tr '[:upper:]' '[:lower:]') prodDomain=$prodDomain
envsubst '${port} ${env} ${domain} ${prodDomain}' < templateVirtualHost.config > /etc/httpd/sites-available/$prodDomain.conf

export port=$stagePortNumber env="stage" domain=$(echo "$devDomain" | tr '[:upper:]' '[:lower:]') prodDomain=$prodDomain
envsubst '${port} ${env} ${domain} ${prodDomain}' < templateVirtualHost.config > /etc/httpd/sites-available/$devDomain.conf

ln -s /etc/httpd/sites-available/$prodDomain.conf /etc/httpd/sites-enabled/$prodDomain.conf
ln -s /etc/httpd/sites-available/$devDomain.conf /etc/httpd/sites-enabled/$devDomain.conf

chmod -R g+srwx,a+rx /var/www/$prodDomain 
chown -R apache:$serviceAccount /var/www/$prodDomain 

systemctl restart httpd


# Configure certbot
certbot --apache -d $prodDomain --redirect
certbot --apache -d $devDomain --redirect

echo "The private key for your service account is:"
cat /home/$serviceAccount/.ssh/id_rsa

echo "The stage port is: $stagePortNumber"
echo "The prod port is: $prodPortNumber"

echo "The service account is: $serviceAccount"
