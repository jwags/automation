#!/bin/bash
# Create database user
echo "Enter prod username"
read prodUsername
echo "Enter dev username"
read devUsername

echo "Enter prod password (max 32 characters)"
read -s prodPassword
echo "Enter dev password (max 32 characters)"
read -s devPassword

echo "Enter prod database name"
read prodDbname
echo "Enter dev database name"
read devDbname

echo "Enter password for root"
read -s rootpassword

# The script will fail at the first error encountered
set -e

#PASS=`pwgen -s 40 1`

mysql -uroot -p$rootpassword <<MYSQL_SCRIPT
CREATE USER '$prodUsername'@'localhost' IDENTIFIED BY '$prodPassword';
GRANT ALL PRIVILEGES ON \`$prodDbname\`.* TO '$prodUsername'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT

mysql -uroot -p$rootpassword <<MYSQL_SCRIPT
CREATE USER '$devUsername'@'localhost' IDENTIFIED BY '$devPassword';
GRANT ALL PRIVILEGES ON \`$devDbname\`.* TO '$devUsername'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT

echo "Prod connection string is"
echo "server=localhost;Database=$prodDbname;user=$prodUsername;password=<password>;"
echo "Dev connection string is"
echo "server=localhost;Database=$devDbname;user=$devUsername;password=<password>;"


# Create user
echo "Enter deployment account name (serviceaccount-1)"
read serviceAccount

echo "Enter Azure Client ID (hjkk34l9-h12j-11aa-1b22b3n3n4n5)"
read -s azureClientId

echo "Enter Azure Client Secret (2j1k34h1kj4h3kj1hkj123h4kj)"
read -s azureClientSecret

adduser $serviceAccount
groupadd deployment-$serviceAccount
usermod -a -G $serviceAccount root
usermod -a -G $serviceAccount apache
usermod -a -G $serviceAccount jwagner
echo "Match User $serviceAccount" >> /etc/ssh/sshd_config
echo -e "\tPasswordAuthentication no" >> /etc/ssh/sshd_config

mkdir /home/$serviceAccount/.ssh
touch /home/$serviceAccount/.ssh/authorized_keys
chmod 700 /home/$serviceAccount/.ssh
chmod 600 /home/$serviceAccount/.ssh/authorized_keys



# Create Virtual Host
echo "Enter production domain (SubDomain.JordansPortfolio.com)"
read prodDomain 

echo "Enter dev domain (SubDomainDev.JordansPortfolio.com)"
read devDomain

echo "Enter first port (5000)"
read firstPort

echo "Enter second port (5001)"
read secondPort



mkdir /var/www/$prodDomain/dev/.secrets
mkdir /var/www/$prodDomain/prod/.secrets
export clientId=$azureClientId clientSecret=$azureClientSecret
envsubst '${clientId} ${clientSecret}' < templateAzurekeyvault.json > /var/www/$prodDomain/dev/.secrets/azurekeyvault.json
envsubst '${clientId} ${clientSecret}' < templateAzurekeyvault.json > /var/www/$prodDomain/prod/.secrets/azurekeyvault.json

chmod 644 /var/www/$prodDomain/dev/.secrets/azurekeyvault.json
chmod 644 /var/www/$prodDomain/prod/.secrets/azurekeyvault.json



mkdir /var/www/$prodDomain
mkdir /var/www/$prodDomain/prod
mkdir /var/www/$prodDomain/prod/wwwroot
mkdir /var/www/$prodDomain/prod/Logs
mkdir /var/www/$prodDomain/prod/Logs/httpd
mkdir /var/www/$prodDomain/prod/Logs/NLog
mkdir /var/www/$prodDomain/prod/Logs/supervisor
mkdir /var/www/$prodDomain/dev
mkdir /var/www/$prodDomain/dev/wwwroot
mkdir /var/www/$prodDomain/dev/Logs
mkdir /var/www/$prodDomain/dev/Logs/httpd
mkdir /var/www/$prodDomain/dev/Logs/NLog
mkdir /var/www/$prodDomain/dev/Logs/supervisor

export port=$firstPort env="prod" domain=$(echo "$prodDomain" | tr '[:upper:]' '[:lower:]') prodDomain=$prodDomain
envsubst '${port} ${env} ${domain} ${prodDomain}' < templateVirtualHost.config > /etc/httpd/sites-available/$prodDomain.conf

export port=$secondPort env="dev" domain=$(echo "$devDomain" | tr '[:upper:]' '[:lower:]') prodDomain=$prodDomain
envsubst '${port} ${env} ${domain} ${prodDomain}' < templateVirtualHost.config > /etc/httpd/sites-available/$devDomain.conf

ln -s /etc/httpd/sites-available/$prodDomain.conf /etc/httpd/sites-enabled/$prodDomain.conf
ln -s /etc/httpd/sites-available/$devDomain.conf /etc/httpd/sites-enabled/$devDomain.conf

chmod -R g+srwx,a+rx /var/www/$prodDomain 
chown -R apache:$serviceAccount /var/www/$prodDomain 

systemctl restart httpd


# Configure certbot
certbot --apache -d $prodDomain --redirect
certbot --apache -d $devDomain --redirect


# Configure Supervisor
echo "Enter prod name for supervisor program (ApplicationName-Prod)"
read supervisorProgramNameForProd

echo "Enter dev name for supervisor program (ApplicationName-Dev)"
read supervisorProgramNameForDev

echo "Enter dll name (ApplicationName.Api.dll)"
read dllName

export programName=$supervisorProgramNameForProd env="prod" domain=$prodDomain port=$firstPort fullEnv="Production" serviceAccount=$serviceAccount dllName=$dllName
envsubst '${programName} ${env} ${domain} ${dllName} ${port} ${fullEnv} ${serviceAccount}' < templateSupervisor.conf > /etc/supervisord.d/$prodDomain.conf

export programName=$supervisorProgramNameForDev env="dev" domain=$prodDomain port=$secondPort fullEnv="Development" serviceAccount=$serviceAccount dllName=$dllName
envsubst '${programName} ${env} ${domain} ${dllName} ${port} ${fullEnv} ${serviceAccount}' < templateSupervisor.conf > /etc/supervisord.d/$devDomain.conf

echo "$serviceAccount ALL=NOPASSWD: /usr/bin/supervisorctl start $supervisorProgramNameForDev, /usr/bin/supervisorctl stop $supervisorProgramNameForDev,  /usr/bin/supervisorctl start $supervisorProgramNameForProd, /usr/bin/supervisorctl stop $supervisorProgramNameForProd" | (EDITOR="tee -a" visudo)

# Configure LogRotate
export domain=$prodDomain env="prod"
envsubst '${domain} ${env}' < templateLogRotate.conf > /etc/logrotate.d/virtualHosts/$prodDomain.conf

export domain=$devDomain env="dev"
envsubst '${domain} ${env}' < templateLogRotate.conf > /etc/logrotate.d/virtualHosts/$devDomain.conf

supervisorctl reread
supervisorctl update

