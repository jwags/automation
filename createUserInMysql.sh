#!/bin/bash

echo "Enter username"
read username

echo "Enter password"
read -s password

echo "Enter database name"
read dbname

echo "Enter password for root"
read -s rootpassword

# The script will fail at the first error encountered
set -e

#PASS=`pwgen -s 40 1`

mysql -uroot -p$rootpassword <<MYSQL_SCRIPT
CREATE USER '$username'@'localhost' IDENTIFIED BY '$password';
GRANT ALL PRIVILEGES ON \`$dbname\`.* TO '$username'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT

echo "MySQL user created."
echo "Connection string is"
echo "server=localhost;Database=$dbname;user=$username;password=<password>;"
