#!/bin/bash
# The script will fail at the first error encountered
set -e

# Create user
echo "Enter deployment account name (deployment-projectname)"
read serviceAccount

adduser $serviceAccount
usermod -a -G $serviceAccount root
usermod -a -G $serviceAccount apache
usermod -a -G $serviceAccount jwagner
echo "Match User $serviceAccount" >> /etc/ssh/sshd_config
echo -e "\tPasswordAuthentication no" >> /etc/ssh/sshd_config

# Setup ssh
mkdir /home/$serviceAccount/.ssh
touch /home/$serviceAccount/.ssh/authorized_keys
chmod 700 /home/$serviceAccount/.ssh
chmod 600 /home/$serviceAccount/.ssh/authorized_keys

ssh-keygen -b 2048 -t rsa -f /home/$serviceAccount/.ssh/id_rsa -q -N ""
chmod 600 /home/$serviceAccount/.ssh/id_rsa
chmod 644 /home/$serviceAccount/.ssh/id_rsa.pub
chown -R $serviceAccount:$serviceAccount /home/$serviceAccount/.ssh

cat /home/$serviceAccount/.ssh/id_rsa.pub >> /home/$serviceAccount/.ssh/authorized_keys


# Create Virtual Host
echo "Enter production domain (SubDomain.JordansPortfolio.com)"
read prodDomain 

echo "Enter stage domain (SubDomainStage.JordansPortfolio.com)"
read devDomain

mkdir /var/www/$prodDomain
mkdir /var/www/$prodDomain/prod
mkdir /var/www/$prodDomain/prod/wwwroot
mkdir /var/www/$prodDomain/prod/Logs
mkdir /var/www/$prodDomain/prod/Logs/httpd
mkdir /var/www/$prodDomain/stage
mkdir /var/www/$prodDomain/stage/wwwroot
mkdir /var/www/$prodDomain/stage/Logs
mkdir /var/www/$prodDomain/stage/Logs/httpd

export port=$firstPort env="prod" domain=$(echo "$prodDomain" | tr '[:upper:]' '[:lower:]') prodDomain=$prodDomain
envsubst '${port} ${env} ${domain} ${prodDomain}' < templateVirtualHost.config > /etc/httpd/sites-available/$prodDomain.conf

export port=$secondPort env="stage" domain=$(echo "$devDomain" | tr '[:upper:]' '[:lower:]') prodDomain=$prodDomain
envsubst '${port} ${env} ${domain} ${prodDomain}' < templateVirtualHost.config > /etc/httpd/sites-available/$devDomain.conf

ln -s /etc/httpd/sites-available/$prodDomain.conf /etc/httpd/sites-enabled/$prodDomain.conf
ln -s /etc/httpd/sites-available/$devDomain.conf /etc/httpd/sites-enabled/$devDomain.conf

chmod -R g+srwx,a+rx /var/www/$prodDomain 
chown -R apache:$serviceAccount /var/www/$prodDomain 

systemctl restart httpd


# Configure certbot
certbot --apache -d $prodDomain --redirect
certbot --apache -d $devDomain --redirect

echo "The private key for your service account is:"
cat /home/$serviceAccount/.ssh/id_rsa